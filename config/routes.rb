Rails.application.routes.draw do

  get "/dbms/engine/readme" => "dbms_engine#index"
  get "/dbms/engine/configure" => "dbms_engine#configure"
  get "/dbms/engine/installer" => "dbms_engine#installer"

  post "/dbms/installer/before_process" => "dbms_installer#before_process"
  post "/dbms/installer/core_process" => "dbms_installer#core_process"
  post "/dbms/installer/post_process" => "dbms_installer#post_process"
  post "/dbms/installer/uninstall" => "dbms_installer#uninstall"

  get "/dbms/connection/status" => "dbms_connection#getConnectionStatus"
  post "/dbms/connection/make" => "dbms_connection#makeNewConnection"
  get "/dbms/connection/tables" => "dbms_connection#getListOfTables"
  post "/dbms/connection/sql" => "dbms_connection#executeSQL"
  post "/dbms/connection/query/store" => "dbms_connection#addStoredQuery"
  get "/dbms/connection/query/all" => "dbms_connection#getListOfStoredQueries"
  get "/dbms/connection/query/one/:id/:cat" => "dbms_connection#getOneStoredQueryInfos"
  put "/dbms/connection/query/update/:id" => "dbms_connection#updateStoredQuery"
  delete "/dbms/connection/query/delete/:id" => "dbms_connection#deleteStoredQuery"

  # Service Route
  get "/ext/dbms/:key" => "dbms_services#execute"

end
