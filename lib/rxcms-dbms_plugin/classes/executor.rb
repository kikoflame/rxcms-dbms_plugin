module RxcmsDbmsPlugin

  class Executor

    # Tag syntax [[<<placeholder>>? &type="stored-query" &data="<<data-source>>" &attrs="<<attr-data>>"...]]
    def self.execute(placeholder, attrs, exts)
      begin

        dbms_config = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path("../../../../config/dbms/dbms_config.yml", __FILE__))))

        if attrs.has_key?('configs')
          if attrs['configs'].has_key?('type')
            if attrs['configs']['type'] == 'stored-query'

              if (ActiveRecord::Base.connection == Database.connection)
                if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_HOST], exts[:appid]] }).nil? &&
                    !Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PORT], exts[:appid]] }).nil? &&
                    !Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_DATABASE], exts[:appid]] }).nil? &&
                    !Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_USER], exts[:appid]] }).nil? &&
                    !Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PASSWORD], exts[:appid]] }).nil?)

                  connectionResult = Database.connect_sql(Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_ADAPTER], exts[:appid]] }).value,
                                                          Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_HOST], exts[:appid]] }).value,
                                                          Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PORT], exts[:appid]] }).value,
                                                          Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_USER], exts[:appid]] }).value,
                                                          Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PASSWORD], exts[:appid]] }).value,
                                                          Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_DATABASE], exts[:appid]] }).value)
                  if (!connectionResult)
                    return "{{[alliance_dbms - #{placeholder.key}] Unable to establish connection to database}}"
                  end
                else
                  return "{{[alliance_dbms - #{placeholder.key}] Missing important keys to establish connection}}"
                end
              end

              if attrs['configs'].has_key?('data')
                tQueryItem = Metadata.all({ :conditions => ['key = ? and sites_id = ?', attrs['configs']['data'], exts[:appid]] })
                languageSwitchable = Metadata.first({ :conditions => ['key = ? and sites_id = ?', 'autoLanguageSwitch', exts[:appid]] })
                queryItem = nil

                tQueryItem.each do |query|
                  tQueryItemJsonObject = ActiveSupport::JSON.decode(SymmetricEncryption.decrypt(query.value))
                  tQueryItemJsonObjectLanguage = tQueryItemJsonObject['language'].scan(/^[a-z]{2}/).first

                  if (!languageSwitchable.nil?)
                    if (languageSwitchable.value == 'yes')
                      if (exts[:language] == tQueryItemJsonObjectLanguage)
                        queryItem = query
                        break
                      end
                    elsif (languageSwitchable.value == 'no')
                      if ('no' == tQueryItemJsonObjectLanguage)
                        queryItem = query
                        break
                      end
                    else
                      return "{{alliance-dbms - #{placeholder.key} the \"value\" for language switchability is invalid}}"
                    end
                  else
                    return "{{alliance-dbms - #{placeholder.key} Language switchability is not available}}"
                  end
                end

                if (!queryItem.nil?)

                  queryItemJsonObject = ActiveSupport::JSON.decode(SymmetricEncryption.decrypt(queryItem.value))
                  queryItemJsonData = Database.connection.select_all(queryItemJsonObject['sql'])

                  if (attrs['configs'].has_key?('tple') && attrs['configs'].has_key?('tplo'))
                    # if it is alternate template
                    contentResult = ''

                    queryItemEvenTemplate = Metadata.find_by_key(attrs['configs']['tple'])
                    queryItemOddTemplate = Metadata.find_by_key(attrs['configs']['tplo'])

                    if (!queryItemEvenTemplate.nil? && !queryItemOddTemplate.nil?)
                      tplCount = 0
                      queryItemJsonData.each do |q|
                        itemTemplate = ''

                        if (tplCount % 2 == 0)
                          itemTemplate = queryItemEvenTemplate.value.strip
                        else
                          itemTemplate = queryItemOddTemplate.value.strip
                        end
                        parsedObjs = itemTemplate.scan(/\[\[\$[a-zA-Z\-_]+\]\]/)

                        parsedObjs.each do |p|
                          tpObj = p.gsub(/[^a-zA-Z\-_]/, '')

                          if (!q[tpObj].nil?)
                            itemTemplate = itemTemplate.gsub(p, q[tpObj].to_s.strip)
                          else
                            itemTemplate = itemTemplate.gsub(p, '')
                          end
                        end

                        contentResult << itemTemplate
                        tplCount = tplCount + 1
                      end
                    end

                    # Content should be empty if even and odd template don't exist
                    contentResult
                  elsif (attrs['configs'].keys.grep(/^tpl[0-9]+$/).size > 0)
                    # if it is multiple template
                    "{{[alliance-dbms - #{placeholder.key}] multiple template is not supported}}"
                  else
                    # if it is single template
                    contentResult = ''

                    queryItemJsonData.each do |q|
                      queryItemTemplate = placeholder.value.strip
                      parsedObjs = queryItemTemplate.scan(/\[\[\$[a-zA-Z\-_]+\]\]/)

                      parsedObjs.each do |p|
                        tpObj = p.gsub(/[^a-zA-Z\-_]/, '')

                        if (!q[tpObj].nil?)
                          queryItemTemplate = queryItemTemplate.gsub(p, q[tpObj].to_s.strip)
                        else
                          queryItemTemplate = queryItemTemplate.gsub(p, '')
                        end
                      end

                      contentResult << queryItemTemplate
                    end

                    # Content should be empty if db returns no data
                    contentResult
                  end
                else
                  placeholder.value.strip.html_safe
                end
              else
                placeholder.value.strip.html_safe
              end
            else
              placeholder.value.strip.html_safe
            end
          else
            placeholder.value.strip.html_safe
          end
        else
          placeholder.value.strip.html_safe
        end
      rescue Exception => ex
        "{{[alliance-dbms - #{placeholder.key}] #{ex.message}}}"
      end
    end

  end
end