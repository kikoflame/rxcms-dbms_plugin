require "rubygems"
require "mysql2"
require "pg"

module RxcmsDbmsPlugin
  extend ::ActiveSupport::Autoload

  autoload :Mysql2
  autoload :PG

  class Engine < ::Rails::Engine
    config.autoload_paths += Dir["#{config.root}/lib/rxcms-dbms_plugin/classes/**/"]
  end
end
