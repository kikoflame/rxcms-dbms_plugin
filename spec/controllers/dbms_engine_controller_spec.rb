require 'spec_helper'

describe DbmsEngineController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')
  end

  describe "#GET index" do
    it 'should show readme page' do
      get :index
      response.body.should be_empty
    end
  end

  describe "#GET configure" do
    before do
      stub_const("Role", double())
      TUserRole = Struct.new(:name)
    end

    it 'should show configure page' do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      TMetadata = Struct.new(:value)

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('admin')
      Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("some value")
      SymmetricEncryption.should_receive(:load!).and_return true
      Database.should_receive(:connect_sql).and_return double()

      get :configure
      response.body.should_not be_empty
    end

    it 'should not show configure page if user is not administrator' do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('user')

      expect { get :configure }.to raise_error
    end

    it 'should not show configure page if request is not xhr' do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return false
      Role.should_receive(:find).and_return TUserRole.new('admin')

      expect { get :configure }.to raise_error
    end
  end

  describe "#GET installer" do
    before do
      stub_const("Role", double())
      TUserRole = Struct.new(:name)
    end

    it 'should show installer page' do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('admin')

      get :installer
      response.body.should_not be_empty
    end

    it 'should not show installer page if user is not administrator' do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('user')

      expect { get :installer }.to raise_error
    end

    it 'should not show installer page if request is not xhr' do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return false
      Role.should_receive(:find).and_return TUserRole.new('admin')

      expect { get :installer }.to raise_error
    end
  end
end