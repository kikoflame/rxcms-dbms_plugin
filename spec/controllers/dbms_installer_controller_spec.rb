require 'spec_helper'

describe DbmsInstallerController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    stub_const("Role", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)
    TUserRole = Struct.new(:name)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')
  end

  it 'before install' do
    SymmetricEncryption.should_receive(:load!).and_return true
    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1

    Role.should_receive(:find).and_return TUserRole.new('admin')
    Metadata.stub(:first) { Metadata }
    Metadata.should_receive(:destroy).at_least(:once).and_return true
    Metadata.should_receive(:create!).at_least(:once).and_return true
    SymmetricEncryption.should_receive(:encrypt).and_return '123456'

    post :before_process, { :dbmsAdapter => 'mysql2', :dbmsHost => 'localhost', :dbmsPort => '3306', :dbmsDatabase => 'test', :dbmsUser => 'user', :dbmsPassword => 'password' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it 'uninstall successfully' do
    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1

    Role.should_receive(:find).and_return TUserRole.new('admin')
    Metadata.stub(:first) { Metadata }
    Metadata.stub(:all) { Metadata }
    Metadata.should_receive(:each).and_return [ Metadata, Metadata ]
    Metadata.should_receive(:destroy).at_least(:once).and_return true

    delete :uninstall

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end
end