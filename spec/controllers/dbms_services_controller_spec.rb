require 'spec_helper'

describe DbmsServicesController do
  before do
    stub_const('Metadata', double())
    TMetadata = Struct.new(:id, :key, :value)
  end

  it 'should provide data services' do
    SymmetricEncryption.should_receive(:load!).and_return true
    Metadata.should_receive(:find_by_key).and_return TMetadata.new(1,'test','test')
    SymmetricEncryption.should_receive(:decrypt).and_return '{"enabled" : true, "sql" : "some sql"}'
    Database.stub(:connection) { Database }
    Database.should_receive(:select_all).and_return []

    get :execute, :key => 'test'

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it 'should not provide data if item is unavailable' do
    SymmetricEncryption.should_receive(:load!).and_return true
    Metadata.should_receive(:find_by_key).and_return TMetadata.new(1,'test','test')
    SymmetricEncryption.should_receive(:decrypt).and_return '{"enabled" : false, "sql" : "some sql"}'

    get :execute, :key => 'test'

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it 'should not provide data if service is unavailable' do
    SymmetricEncryption.should_receive(:load!).and_return true
    Metadata.should_receive(:find_by_key).and_return nil

    get :execute, :key => 'test'

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end
end