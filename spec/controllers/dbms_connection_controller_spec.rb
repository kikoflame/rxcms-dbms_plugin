require 'spec_helper'

describe DbmsConnectionController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    stub_const("Role", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)
    TUserRole = Struct.new(:name)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
  end

  it 'should make new connections' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    SymmetricEncryption.should_receive(:load!).and_return true
    SymmetricEncryption.should_receive(:encrypt).and_return('123456')
    Database.should_receive(:connect_sql).and_return true

    post :makeNewConnection, { :type => 'mysql2', :host => 'localhost', :port => '3306', :user => 'root', :password => '123456', :database => 'test' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should eq('success')
  end

  it 'should not make new connections if there is an unauthorized user' do
    Role.should_receive(:find).and_return TUserRole.new('user')
    SymmetricEncryption.should_receive(:load!).and_return true

    post :makeNewConnection, { :type => 'mysql2', :host => 'localhost', :port => '3306', :user => 'root', :password => '123456', :database => 'test' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should eq('failure')
  end

  it 'should not make new connections if server dont exist' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    SymmetricEncryption.should_receive(:load!).and_return true
    SymmetricEncryption.should_receive(:encrypt).and_return('123456')
    Database.should_receive(:connect_sql).and_return false

    post :makeNewConnection, { :type => 'mysql2', :host => 'localhost', :port => '3306', :user => 'root', :password => '123456', :database => 'test' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should eq('failure')
  end

  it 'should get connection status' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TConn = Struct.new(:connection)
    TConnAttrs = Struct.new(:active?, :current_database)
    connection1 = TConnAttrs.new(true, 'test')
    connection2 = TConnAttrs.new(true, 'testtwo')

    ActiveRecord::Base.should_receive(:connection).at_least(:once).and_return TConn.new(connection1)
    Database.should_receive(:connection).at_least(:once).and_return TConn.new(connection2)
    Database.connection.should_receive(:active?).and_return true

    get :getConnectionStatus

    response.should be_success
  end

  it 'should get connection status if connection is unavailable' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TConn = Struct.new(:connection)
    TConnAttrs = Struct.new(:active?, :current_database)
    connection1 = TConnAttrs.new(true, 'test')
    connection2 = TConnAttrs.new(true, 'testtwo')

    ActiveRecord::Base.should_receive(:connection).at_least(:once).and_return TConn.new(connection1)
    Database.should_receive(:connection).at_least(:once).and_return TConn.new(connection2)
    Database.connection.should_receive(:active?).and_return false

    get :getConnectionStatus

    response.should be_success
  end

  it 'should get list of tables of database' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TConn = Struct.new(:connection)
    TConnAttrs = Struct.new(:active?, :current_database)
    connection1 = TConnAttrs.new(true, 'test')
    connection2 = TConnAttrs.new(true, 'testtwo')

    ActiveRecord::Base.should_receive(:connection).at_least(:once).and_return TConn.new(connection1)
    Database.should_receive(:connection).at_least(:once).and_return TConn.new(connection2)
    Database.connection.should_receive(:tables).and_return []

    get :getListOfTables

    response.should be_success
  end

  it 'should get list of tables of database if user is admin' do
    Role.should_receive(:find).and_return TUserRole.new('user')

    get :getListOfTables

    response.should be_success
  end

  it 'should execute sql' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TConn = Struct.new(:connection)
    TConnAttrs = Struct.new(:active?, :current_database)
    connection1 = TConnAttrs.new(true, 'test')
    connection2 = TConnAttrs.new(true, 'testtwo')

    ActiveRecord::Base.should_receive(:connection).at_least(:once).and_return TConn.new(connection1)
    Database.should_receive(:connection).at_least(:once).and_return TConn.new(connection2)
    Database.connection.should_receive(:execute).at_least(:once).and_return true
    Database.connection.should_receive(:select_all).and_return []

    post :executeSQL, :sql => 'test'

    response.should be_success
  end

  it 'should execute sql when no method error is raised' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TConn = Struct.new(:connection)
    TConnAttrs = Struct.new(:active?, :current_database)
    connection1 = TConnAttrs.new(true, 'test')
    connection2 = TConnAttrs.new(true, 'testtwo')

    ActiveRecord::Base.should_receive(:connection).at_least(:once).and_return TConn.new(connection1)
    Database.should_receive(:connection).at_least(:once).and_return TConn.new(connection2)
    Database.connection.should_receive(:execute).at_least(:once).and_return true
    Database.connection.should_receive(:select_all) { raise NoMethodError }

    post :executeSQL, :sql => 'test'

    response.should be_success
  end

  it 'should fail to execute sql when error is raised' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TConn = Struct.new(:connection)
    TConnAttrs = Struct.new(:active?, :current_database)
    connection1 = TConnAttrs.new(true, 'test')
    connection2 = TConnAttrs.new(true, 'testtwo')

    ActiveRecord::Base.should_receive(:connection).at_least(:once).and_return TConn.new(connection1)
    Database.should_receive(:connection).at_least(:once).and_return TConn.new(connection2)
    Database.connection.should_receive(:execute).at_least(:once).and_return true
    Database.connection.should_receive(:select_all) { raise }

    post :executeSQL, :sql => 'test'

    response.should be_success
  end

  it "should fail to execute sql if user is not admin" do
    Role.should_receive(:find).and_return TUserRole.new('user')

    post :executeSQL, :sql => 'test'

    response.should be_success
  end

  it 'should get list of stored queries' do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TStoredQuery = Struct.new(:id,:key,:value)

    SymmetricEncryption.should_receive(:decrypt).at_least(:once).and_return '123456'
    Metadata.should_receive(:all).and_return [TStoredQuery.new(1,'test','test')]

    get :getListOfStoredQueries

    response.should be_success
  end

  it 'should get one stored query info'  do
    SymmetricEncryption.should_receive(:load!).and_return true

    Role.should_receive(:find).and_return TUserRole.new('admin')
    TMetadata = Struct.new(:id, :key, :value)
    Metadata.should_receive(:first).and_return TMetadata.new(1,'test','test')
    SymmetricEncryption.should_receive(:decrypt).and_return '123456'

    get :getOneStoredQueryInfos, { :id => 1, :cat => 'test' }

    response.should be_success
  end

  it 'should add stored query' do
    SymmetricEncryption.should_receive(:load!).and_return true

    Role.should_receive(:find).and_return TUserRole.new('admin')
    TMetadata = Struct.new(:id, :key, :value)

    Metadata.stub(:first) { Metadata }
    Metadata.should_receive(:nil?).and_return double()
    Metadata.should_receive(:all).and_return [TMetadata.new(1, 'test', '{"sql" : "not test"}')]
    SymmetricEncryption.should_receive(:decrypt).at_least(:once).and_return '{"sql" : "not test"}'
    SymmetricEncryption.should_receive(:encrypt).and_return '123456'
    # ActiveSupport::JSON.should_receive(:decode).at_least(:once).and_return {{ 'sql' => 'not test' }}
    Metadata.should_receive(:create).and_return true

    post :addStoredQuery, { :key => 'test', :value => 'ack' }

    response.should be_success
  end

  it 'should update stored query' do
    SymmetricEncryption.should_receive(:load!).and_return true
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TMetadata = Struct.new(:id, :key, :value)

    Metadata.should_receive(:find).and_return TMetadata.new(1,'test','test')
    SymmetricEncryption.should_receive(:decrypt).and_return '{"enable" => true, "language" => "en", "sql" => "good"}'
    ActiveSupport::JSON.should_receive(:decode).at_least(:once).and_return {{"enable" => true, "language" => "en", "sql" => "good"}}
    SymmetricEncryption.should_receive(:encrypt).and_return 'value'
    Metadata.should_receive(:update).and_return true
    put :updateStoredQuery, { :id => 1, :value => 'new value', :enabled => nil, :language => nil }

    response.should be_success
  end

  it 'should update stored query for other values' do
    SymmetricEncryption.should_receive(:load!).and_return true
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TMetadata = Struct.new(:id, :key, :value)

    Metadata.should_receive(:find).and_return TMetadata.new(1,'test','test')
    SymmetricEncryption.should_receive(:decrypt).and_return '{"enable" => true, "language" => "en", "sql" => "good"}'
    ActiveSupport::JSON.should_receive(:decode).at_least(:once).and_return {{"enable" => true, "language" => "en", "sql" => "good"}}
    SymmetricEncryption.should_receive(:encrypt).and_return 'value'
    Metadata.should_receive(:update).and_return true
    put :updateStoredQuery, { :id => 1, :value => nil, :enabled => true, :language => nil }

    response.should be_success
  end

  it 'should update stored query for other last values' do
    SymmetricEncryption.should_receive(:load!).and_return true
    Role.should_receive(:find).and_return TUserRole.new('admin')
    TMetadata = Struct.new(:id, :key, :value)

    Metadata.should_receive(:find).and_return TMetadata.new(1,'test','test')
    SymmetricEncryption.should_receive(:decrypt).and_return '{"enable" => true, "language" => "en", "sql" => "good"}'
    ActiveSupport::JSON.should_receive(:decode).at_least(:once).and_return {{"enable" => true, "language" => "en", "sql" => "good"}}
    SymmetricEncryption.should_receive(:encrypt).and_return 'value'
    Metadata.should_receive(:update).and_return true
    put :updateStoredQuery, { :id => 1, :value => nil, :enabled => nil, :language => 'vi' }

    response.should be_success
  end

  it "should delete stored query" do
    Role.should_receive(:find).and_return TUserRole.new('admin')
    Metadata.stub(:find_by_id) {Metadata}
    Metadata.should_receive(:destroy).and_return true

    delete :deleteStoredQuery, :id => 1

    response.should be_success
  end
end