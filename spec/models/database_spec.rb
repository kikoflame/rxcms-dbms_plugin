require 'spec_helper'

describe Database do
  it 'should connect to SQL server successfully' do
    TConn = Struct.new(:connection)

    SymmetricEncryption.should_receive(:load!).and_return true
    SymmetricEncryption.should_receive(:decrypt).and_return '12345'
    Database.should_receive(:establish_connection).and_return TConn.new(double())

    Database.connect_sql('mysql2','localhost','3306','admin','','test').should == true
  end

  it 'should not connect to SQL server successfully if there are no connections' do
    TConn = Struct.new(:connection)

    SymmetricEncryption.should_receive(:load!).and_return true
    SymmetricEncryption.should_receive(:decrypt).and_return '12345'
    Database.should_receive(:establish_connection).and_return TConn.new(nil)
    Database.stub(:connection).and_return nil

    Database.connect_sql('mysql2','localhost','3306','admin','','test').should == false
  end

  it 'should connect to SQLite' do
    TConn = Struct.new(:connection)

    Database.should_receive(:establish_connection).and_return TConn.new(nil)
    Database.stub(:connection).and_return double()

    Database.connect_sqlite('some/path/database.db').should == true
  end

  it 'should not connect to SQLite if connection is unavailable' do
    TConn = Struct.new(:connection)

    Database.should_receive(:establish_connection).and_return TConn.new(nil)
    Database.stub(:connection).and_return nil

    Database.connect_sqlite('some/path/database.db').should == false
  end
end