require 'spec_helper'

describe RxcmsDbmsPlugin::Executor do
  before do
    stub_const('Metadata', double())
    stub_const('Database', double())
    TMetadata = Struct.new(:id, :key, :value)
    TConnection = Struct.new(:connection)
  end

  it "should execute if normal" do
    ActiveRecord::Base.stub(:connection) { TConnection.new(true) }
    Database.stub(:connection) { TConnection.new(true) }
    Database.stub(:connect_sql) { double() }
    Metadata.stub(:first) { TMetadata.new(1,'test','yes') }
    Metadata.should_receive(:all).and_return [TMetadata.new(1,'test','test')]
    SymmetricEncryption.should_receive(:decrypt).at_least(:once).and_return '{"language":"en_US"}'
    Database.stub(:connection) { Database }
    Database.should_receive(:select_all).and_return [{ 'name' => 'test' }, {'name' => 'test'}]

    result = RxcmsDbmsPlugin::Executor.execute(TMetadata.new(1,'test','[[$name]]'),{'configs' => { 'type' => 'stored-query', 'data' => 'test' }},{ :appid => 1, :language => 'en' })

    result.should_not be_empty
  end

  it "should execute if normal and with tple and tplo" do
    ActiveRecord::Base.stub(:connection) { TConnection.new(true) }
    Database.stub(:connection) { TConnection.new(true) }
    Database.stub(:connect_sql) { double() }
    Metadata.stub(:first) { TMetadata.new(1,'test','no') }
    Metadata.should_receive(:all).and_return [TMetadata.new(1,'name','test'), TMetadata.new(2,'name','test')]
    SymmetricEncryption.should_receive(:decrypt).at_least(:once).and_return '{"language":"no"}'
    Database.stub(:connection) { Database }
    Database.should_receive(:select_all).and_return [{ 'name' => 'test' }, {'name' => 'test'}]
    Metadata.should_receive(:find_by_key).at_least(:once).and_return TMetadata.new(1,'evenandodd','[[$name]]')

    result = RxcmsDbmsPlugin::Executor.execute(TMetadata.new(1,'test','test'),{'configs' => { 'type' => 'stored-query', 'data' => 'test', 'tple' => 'even', 'tplo' => 'odd' }},{ :appid => 1, :language => 'en' })

    result.should_not be_empty
  end
end