class DbmsConnectionController < ApplicationController
  include DbmsPluginHelper

  # Only allow admin and developer
  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/dbms/dbms_config.yml', __FILE__))))

  # Controller Actions
  def makeNewConnection
    SymmetricEncryption.load!

    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      connectionResult = false

      if (params[:type] == "sqlite")
        connectionResult = Database.connect_sqlite(params[:database])
      else
        connectionResult = Database.connect_sql(params[:type], params[:host], params[:port], params[:user], SymmetricEncryption.encrypt(params[:password]), params[:database])
      end

      if (connectionResult)
        render :json => {:status => "success"}
      else
        render :json => {:status => "failure"}
      end
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def getConnectionStatus
    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      if (ActiveRecord::Base.connection != Database.connection)
        status = Database.connection.active?

        if (status)
          render :json => {:status => "success", :data => Database.connection.current_database}
        else
          render :json => {:status => "failure", :message => "database unavailable"}
        end
      else
        render :json => {:status => "failure", :message => "connection unavailable"}
      end
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def getListOfTables
    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      if (ActiveRecord::Base.connection != Database.connection)
        tables = Database.connection.tables
        render :json => {:status => "success", :data => tables}
      else
        render :json => {:status => "success", :message => []}
      end
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def executeSQL
    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      if (ActiveRecord::Base.connection != Database.connection)

        resultSet = []
        begin
          Database.connection.execute("BEGIN")
          resultSet = Database.connection.select_all(params[:sql])
          # Must put ROLLBACK here because Postgres adapter doesn't throw any exception when update or delete commands are executed
          Database.connection.execute("ROLLBACK")

          # Parse columns
          columns = Array.new
          if resultSet.length > 0
            # Take this first one
            firstResult = resultSet.first
            columns = firstResult.keys
          end

          render :json => {:status => "success", :data => {:columns => columns, :result => resultSet}}
        rescue NoMethodError
          Database.connection.execute("ROLLBACK")
          render :json => {:status => "success", :data => {:columns => [], :result => []}}
        rescue ActiveRecord::StatementInvalid => dberr
          render :json => { :status => "failure", :data => {:columns => [], :result => []}, :message => dberr.message }
        end

      else
        render :json => {:status => "failure", :data => {:columns => [], :result => []}, :message => "connect not established"}
      end
    rescue Exception => ex
      render :json => {:status => "failure", :data => {:columns => [], :result => []}, :message => "command executed, no result set returned"}
    end
  end

  def getListOfStoredQueries
    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      storedQueries = Metadata.all({ :conditions => ['cat = ? and sites_id = ?', APP_CONFIG[:DBMS_CAT_STORED_QUERY], session[:accessible_appid]] })

      tArray = Array.new
      storedQueries.each do |t|
        tHash = Hash.new

        tHash[:id] = t.id
        tHash[:key] = t.key
        tHash[:value] = SymmetricEncryption.decrypt(t.value)

        tArray << tHash
      end

      render :json => {:status => "success", :data => tArray}
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def getOneStoredQueryInfos
    SymmetricEncryption.load!

    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      storedQuery = Metadata.first({ :conditions => [ 'id = ? and cat = ? and sites_id = ?', params[:id], params[:cat], session[:accessible_appid] ]})

      render :json => {:status => "success", :data => {:id => storedQuery.id ,:key => storedQuery.key, :value => SymmetricEncryption.decrypt(storedQuery.value)}}
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def addStoredQuery
    SymmetricEncryption.load!

    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      # if (Metadata.all(:conditions => ["key = ?", params[:key]]).length > 0)
      #  raise "key is duplicated"
      # end

      if (!Metadata.first({ :conditions => ['key = ?', params[:key]]}).nil?)
        raise "key is duplicated"
      end

      # Check for value duplications
      storedQueryValues = Metadata.all(:conditions => ["cat = ? and sites_id = ?", APP_CONFIG[:DBMS_CAT_STORED_QUERY], session[:accessible_appid]])
      storedQueryValues.each do |t|
        valueJsonObject = ActiveSupport::JSON.decode(SymmetricEncryption.decrypt(t.value))

        if valueJsonObject['sql'] == params[:value]
          raise "value is duplicated"
        end
      end

      # If all is fine, create the stored query
      Metadata.create({
                          :key => params[:key],
                          :value => SymmetricEncryption.encrypt({ :sql => params[:value], :enabled => false, :language => 'none' }.to_json),
                          :cat => APP_CONFIG[:DBMS_CAT_STORED_QUERY],
                          :mime => "text/sql",
                          :sites_id => session[:accessible_appid]
                      })

      render :json => {:status => "success"}
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def updateStoredQuery
    SymmetricEncryption.load!

    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      # select that id first
      updateObj = Metadata.find(params[:id])

      if (!updateObj.nil?)
        updateObjData = ActiveSupport::JSON.decode(SymmetricEncryption.decrypt(updateObj.value))
        if (!params[:value].nil? && params[:enabled].nil? && params[:language].nil?)
          Metadata.update(params[:id], {
              :value => SymmetricEncryption.encrypt({ :sql => params[:value], :enabled => updateObjData['enabled'], :language => updateObjData['language'] }.to_json)
          });
        elsif (params[:value].nil? && !params[:enabled].nil? && params[:language].nil?)
          Metadata.update(params[:id], {
              :value => SymmetricEncryption.encrypt({ :sql => updateObjData['sql'] , :enabled => params[:enabled] == "true" ? true : false, :language => updateObjData['language'] }.to_json)
          });
        elsif (params[:value].nil? && params[:enabled].nil? && !params[:language].nil?)
          Metadata.update(params[:id], {
              :value => SymmetricEncryption.encrypt({ :sql => updateObjData['sql'] , :enabled => updateObjData['enabled'], :language => params[:language] }.to_json)
          });
        else
          # Unimplemented
        end
      end

      render :json => {:status => "success"}
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end

  def deleteStoredQuery
    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      Metadata.find_by_id(params[:id]).destroy

      render :json => {:status => "success"}
    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message}
    end
  end
  # END

  private

end