class DbmsServicesController < ApplicationController

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/dbms/dbms_config.yml', __FILE__))))

  # Controller Actions
  def execute
    SymmetricEncryption.load!

    begin
      serviceObj = Metadata.find_by_key(params[:key])

      if (!serviceObj.nil?)
        serviceValueObj = ActiveSupport::JSON.decode(SymmetricEncryption.decrypt(serviceObj.value))

        if (serviceValueObj['enabled'] == true)
          resultSet = Database.connection.select_all(serviceValueObj['sql'])

          render :json => {:status => "success", :data => resultSet}
        else
          raise "service unavailable"
        end
      else
        raise "data unavailable"
      end

    rescue Exception => ex
      render :json => {:status => "failure", :message => ex.message, :data => []}
    end
  end
  # END

end