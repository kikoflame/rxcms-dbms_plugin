class DbmsEngineController < ApplicationController
  include DbmsPluginHelper, DbmsEngineHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/dbms/dbms_config.yml', __FILE__))))

  # Allow all to access index
  # Disallow all to access configure and installer, except for admin

  # Write your readme here
  def index

  end

  def configure

    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    if request.xhr?
      if Metadata.first({ :conditions => [ 'key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).nil?
        raise "setup required"
      else
        if Metadata.first({ :conditions => [ 'key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).value.empty?
          raise "setup required"
        else
          # Perform connection persisting if necessary
          persist_connection

          respond_to do |t|
            t.html
          end
        end
      end
    else
      raise 'unauthorized access'
    end
  end

  def installer

    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end

  private

end
