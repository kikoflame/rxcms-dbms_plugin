class DbmsInstallerController < ApplicationController
  include DbmsPluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/dbms/dbms_config.yml', __FILE__))))

  # Each step should return JSON status "success", "failure" or "unimplemented"

  # Used for initializing and creating database entries
  def before_process
    SymmetricEncryption.load!

    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      if !Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_ADAPTER], session[:accessible_appid]] }).nil?
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_ADAPTER], session[:accessible_appid]] }).destroy
      end
      if !Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_HOST], session[:accessible_appid]] }).nil?
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_HOST], session[:accessible_appid]] }).destroy
      end
      if !Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_PORT], session[:accessible_appid]] }).nil?
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_PORT], session[:accessible_appid]] }).destroy
      end
      if !Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).nil?
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).destroy
      end
      if !Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_USER], session[:accessible_appid]] }).nil?
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_USER], session[:accessible_appid]] }).destroy
      end
      if !Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_PASSWORD], session[:accessible_appid]] }).nil?
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_PASSWORD], session[:accessible_appid]] }).destroy
      end

      Metadata.create!({
        :key => APP_CONFIG[:DBMS_CURRENT_ADAPTER],
        :value => params[:dbmsAdapter].nil? ? '' : params[:dbmsAdapter],
        :cat => APP_CONFIG[:DBMS_CAT_CONFIG],
        :mime => "text/plain",
        :sites_id => session[:accessible_appid]
      })
      Metadata.create!({
        :key => APP_CONFIG[:DBMS_CURRENT_HOST],
        :value => params[:dbmsHost].nil? ? '' : params[:dbmsHost],
        :cat => APP_CONFIG[:DBMS_CAT_CONFIG],
        :mime => "text/plain",
        :sites_id => session[:accessible_appid]
      })
      Metadata.create!({
        :key => APP_CONFIG[:DBMS_CURRENT_PORT],
        :value => params[:dbmsPort].nil? ? '' : params[:dbmsPort],
        :cat => APP_CONFIG[:DBMS_CAT_CONFIG],
        :mime => "text/plain",
        :sites_id => session[:accessible_appid]
      })
      Metadata.create!({
        :key => APP_CONFIG[:DBMS_CURRENT_DATABASE],
        :value => params[:dbmsDatabase].nil? ? '' : params[:dbmsDatabase],
        :cat => APP_CONFIG[:DBMS_CAT_CONFIG],
        :mime => "text/plain",
        :sites_id => session[:accessible_appid]
      })
      Metadata.create!({
        :key => APP_CONFIG[:DBMS_CURRENT_USER],
        :value => params[:dbmsUser].nil? ? '' : params[:dbmsUser],
        :cat => APP_CONFIG[:DBMS_CAT_CONFIG],
        :mime => "text/plain",
        :sites_id => session[:accessible_appid]
      })
      Metadata.create!({
        :key => APP_CONFIG[:DBMS_CURRENT_PASSWORD],
        :value => SymmetricEncryption.encrypt(params[:dbmsPassword].nil? ? '' : params[:dbmsPassword]),
        :cat => APP_CONFIG[:DBMS_CAT_CONFIG],
        :mime => "text/plain",
        :sites_id => session[:accessible_appid]
      })

      render :json => { :status => 'success' }
    rescue Exception => ex
      render :json => { :status => 'failure', :message => ex.message }
    end
  end

  # Used for logical processing
  def core_process
    render :json => { :status => 'unimplemented' }
  end

  # Used for configuring data
  def post_process
    render :json => { :status => 'unimplemented' }
  end

  # Uninstaller
  def uninstall
    begin

      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      # Delete all configuration items
      Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_ADAPTER], session[:accessible_appid]] }).destroy
      Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_HOST], session[:accessible_appid]] }).destroy
      Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_PORT], session[:accessible_appid]] }).destroy
      Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).destroy
      Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_USER], session[:accessible_appid]] }).destroy
      Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:DBMS_CURRENT_PASSWORD], session[:accessible_appid]] }).destroy

      # Delete all stored queries
      Metadata.all({ :conditions => ['cat = ? and sites_id = ?', APP_CONFIG[:DBMS_CAT_STORED_QUERY], session[:accessible_appid]] }).each do |t|
        t.destroy
      end

      render :json => { :status => 'success' }
    rescue
      render :json => { :status => 'failure' }
    end
  end

  private

end
