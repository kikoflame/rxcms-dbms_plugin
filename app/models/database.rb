class Database < ActiveRecord::Base
  @@conn = nil

  def self.connect_sql(adapter, host, port, username, password, database)
    SymmetricEncryption.load!
    # Load configuration items (MANDATORY, must be included)
    app_config = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/dbms/dbms_config.yml', __FILE__))))

    begin
      decryptedPassword = SymmetricEncryption.decrypt(password)

      establish_connection(
          :adapter  => adapter,
          :host     => host,
          :port     => port.to_i,
          :username => username,
          :password => decryptedPassword,
          :database => database
      )

      if (self.connection.nil?)
        return false
      else
        return true
      end
    rescue
      return false
    end
  end

  def self.connect_sqlite(path)
    begin
      establish_connection(
          :adapter => "sqlite3",
          :database => path
      )

      if (self.connection.nil?)
        return false
      else
        return true
      end
    rescue
      return false
    end
  end
end
