$(function(){

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    $("#dbmsTypeSelect").on("change", function(){
        $(".connectorContainer").fadeOut("fast");
        switch($(this).val())
        {
            case "mysql2":
                $("#sql").fadeIn("fast");
                $("#portTxt").val("3306");
                break;
            case "sqlite":
                $("#sqlite").fadeIn("fast");
                break;
            case "postgresql":
                $("#sql").fadeIn("fast");
                $("#portTxt").val("5432");
                break;
            default:
        }
    });

    $(".sqlProceedBtn").on("click", function(){

        if ($("#dbmsTypeSelect").val() == "sqlite")
        {
            if ($("#dbTxtSqlite").val().length == 0)
            {
                Messenger().post("Path to database is required");
                return;
            }
        }
        else
        {
            var errors = "";
            if ($("#hostTxt").val().length == 0) errors += "Host is required.<br />"
            if ($("#portTxt").val().length == 0) errors += "Port is required.<br />"
            if ($("#dbTxt").val().length == 0) errors += "Database is required.<br />"
            if ($("#userTxt").val().length == 0) errors += "User is required.<br />"

            if (errors.length > 0)
            {
                Messenger().post(errors);
                return;
            }
        }

        $("#sqlProceedBtn").attr("disabled","disabled");

        $.post("/dbms/connection/make", {
            "type" : $("#dbmsTypeSelect").val(),
            "host" : $("#hostTxt").val(),
            "port" : $("#portTxt").val(),
            "database": $("#dbmsTypeSelect").val() == "sqlite" ? $("#dbTxtSqlite").val() : $("#dbTxt").val(),
            "user" : $("#userTxt").val(),
            "password" : $("#passwordTxt").val()
        }, function(response){
            if (response.status == "success")
            {
                Messenger().post("Connection to database server was established, creating persistent database records...");

                $.post("/dbms/installer/before_process", {
                    "dbmsAdapter" : $("#dbmsTypeSelect").val(),
                    "dbmsHost" : $("#hostTxt").val(),
                    "dbmsPort" : $("#portTxt").val(),
                    "dbmsDatabase" : $("#dbmsTypeSelect").val() == "sqlite" ? $("#dbTxtSqlite").val() : $("#dbTxt").val(),
                    "dbmsUser" : $("#userTxt").val(),
                    "dbmsPassword" : $("#passwordTxt").val()
                }, function(response){
                    if (response.status == "success")
                    {
                        Messenger().post("All done, redirecting...");
                        location.reload();
                    }
                    else
                    {
                        $("#sqlProceedBtn").removeAttr("disabled");
                        Messenger().post("There was a problem while doing initial processing");
                    }
                }).error(function(){
                {
                    $("#sqlProceedBtn").removeAttr("disabled");
                    Messenger().post("Unable to do initial processing");
                }
                })
            } else
            {
                $("#sqlProceedBtn").removeAttr("disabled");
                Messenger().post("Connection cannot be established due to either wrong login information or server unavailability");
            }
        }).error(function(){
            $("#sqlProceedBtn").removeAttr("disabled");
            Messenger().post("Unable to contact server");
        });

    });
});