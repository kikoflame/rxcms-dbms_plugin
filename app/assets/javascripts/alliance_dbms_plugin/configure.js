var const_key_regex = /^[a-zA-Z\-]+$/i;

function reloadStoredQueriesList()
{
    $.getJSON("/dbms/connection/query/all", function(response){
        if (response.status == "success")
        {
            // Get previous selection
            var prevActiveObj = $($(".storedQuery").filter(function(){
                return $(this).hasClass("active");
            }).first());
            var prevActiveObjKey = null;
            if (prevActiveObj != null && prevActiveObj != undefined)
            {
                prevActiveObjKey = $(prevActiveObj.find("span:first")).text();
            }

            $("#storeQueryList li:not(:first)").remove();
            for (var i = 0; i < response.data.length; i++)
            {
                var cloned = $("#storeQueryDummy").clone();

                $(cloned.find("a:first span:first")).text(response.data[i].key);
                var responseData = JSON.parse(response.data[i].value);
                $(cloned.find("a:first input[type=checkbox]:first")).attr("checked", responseData.enabled);
                $(cloned.find("a:first input[type=checkbox]:first")).on("click", function(e){
                    e.stopPropagation();
                    var checked = $(this).is(":checked").toString();

                    var id = $($(this).parents("li:first")).attr("data-store-id");

                    $.ajax({
                        url: "/dbms/connection/query/update/" + id,
                        cache: false,
                        type: "PUT",
                        data: {
                            "enabled" : checked
                        },
                        before_send: function(){

                        },
                        error: function(){
                            Messenger().post("Unable to contact server");
                        },
                        success: function(response){
                            if (response.status == "success")
                            {
                                Messenger().post("The stored query was successfully updated");
                            }
                            else
                            {
                                Messenger().post("Unable to update the stored query");
                            }
                        }
                    });
                });
                $(cloned.find("a:first span:eq(1)")).on("click", function(e){
                    e.stopPropagation();

                    var $this = $(this);
                    var id = $($(this).parents("li:first")).attr("data-store-id");

                    $.ajax({
                        url: "/dbms/connection/query/delete/" + id,
                        cache: false,
                        type: "DELETE",
                        before_send: function(){

                        },
                        error: function(){
                            Messenger().post("Unable to contact server");
                        },
                        success: function(response){
                            if (response.status == "success")
                            {
                                var activeQueryObj = $($(".storedQuery").filter(function(){
                                    return $(this).hasClass("active");
                                }).first());

                                if ($(activeQueryObj).find("span:eq(1)").get(0) == $this.get(0))
                                {
                                     $("#updateSqlBtn").attr("disabled","disabled");
                                     $("#updateSqlBtn").hide();
                                     $("#saveSqlBtn").show();
                                     $($("#resultTable thead:first").find("tr:first")).html("");
                                     $("#resultTable tbody:first").html("");
                                }

                                reloadStoredQueriesList();
                                Messenger().post("The stored query was successfully deleted");
                            }
                            else
                            {
                                Messenger().post("Unable to delete the stored query");
                            }
                        }
                    });
                });
                cloned.attr("data-store-id", response.data[i].id);
                cloned.attr("data-store-language", responseData.language);
                cloned.addClass("storedQuery");
                cloned.removeAttr("id");
                cloned.css("display","block");

                // set active if equal
                if (prevActiveObjKey != null)
                    if ($(cloned.find("span:first")).text() == prevActiveObjKey)
                        cloned.addClass("active");

                $("#storeQueryList").append(cloned);

            }
        }
        else
        {
            Messenger().post("unable to get list of stored queries");
        }
    });
}

$(function(){

    var elem = $("#saveSqlBtn");
    var updateElem = $("#updateSqlBtn");
    $.data(elem, "sqlResult", 0);
    $.data(updateElem, "sqlResult", 0);

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    $("#dbmsTypeSelect").on("change", function(){
        $(".connectorContainer").fadeOut("fast");
        switch($(this).val())
        {
            case "mysql2":
                $("#sql").fadeIn("fast");
                $("#portTxt").val("3306");
                break;
            case "sqlite":
                $("#sqlite").fadeIn("fast");
                break;
            case "postgresql":
                $("#sql").fadeIn("fast");
                $("#portTxt").val("5432");
                break;
            default:
        }
    });

    $(".sqlProceedBtn").on("click", function(){

        if ($("#dbmsTypeSelect").val() == "sqlite")
        {
            if ($("#dbTxtSqlite").val().length == 0)
            {
                Messenger().post("Path to database is required");
                return;
            }
        }
        else
        {
            var errors = "";
            if ($("#hostTxt").val().length == 0) errors += "Host is required.<br />"
            if ($("#portTxt").val().length == 0) errors += "Port is required.<br />"
            if ($("#dbTxt").val().length == 0) errors += "Database is required.<br />"
            if ($("#userTxt").val().length == 0) errors += "User is required.<br />"

            if (errors.length > 0)
            {
                Messenger().post(errors);
                return;
            }
        }

        $("#sqlProceedBtn").attr("disabled","disabled");

        $.post("/dbms/connection/make",
        {
            "type" : $("#dbmsTypeSelect").val(),
            "host" : $("#hostTxt").val(),
            "port" : $("#portTxt").val(),
            "database": $("#dbmsTypeSelect").val() == "sqlite" ? $("#dbTxtSqlite").val() : $("#dbTxt").val(),
            "user" : $("#userTxt").val(),
            "password" : $("#passwordTxt").val()
        }
        ,function(response){
            if (response.status == "success")
            {
                Messenger().post("Connection to database server was established, creating persistent database records...");

                $.post("/dbms/installer/before_process", {
                    "dbmsAdapter" : $("#dbmsTypeSelect").val(),
                    "dbmsHost" : $("#hostTxt").val(),
                    "dbmsPort" : $("#portTxt").val(),
                    "dbmsDatabase" : $("#dbmsTypeSelect").val() == "sqlite" ? $("#dbTxtSqlite").val() : $("#dbTxt").val(),
                    "dbmsUser" : $("#userTxt").val(),
                    "dbmsPassword" : $("#passwordTxt").val()
                }, function(response){
                    if (response.status == "success")
                    {
                        Messenger().post("All done, reloading...");
                        location.reload();
                    }
                    else
                    {
                        $("#sqlProceedBtn").removeAttr("disabled");
                        Messenger().post("There was a problem while doing initial processing");
                    }
                }).error(function(){
                        $("#sqlProceedBtn").removeAttr("disabled");
                        Messenger().post("Unable to do initial processing");
                    })
            }
            else
            {
                Messenger().post("The current login information is invalid or component configuration items are malformed.");
                location.reload();
            }
        }).error(function(){
            $("#sqlProceedBtn").removeAttr("disabled");
            Messenger().post("Unable to connect to server");
        });

        //$("#dataManagementConsole").fadeIn("fast");
    });

    // Populate stored query list
    reloadStoredQueriesList();

    // Retrieve data from table
    $("#executeSqlBtn").on("click", function(){
        var $this = $(this);
        $($("#resultTable thead:first").find("tr:first")).html("");
        $("#resultTable tbody:first").html("");

        if ($("#sqlQueryTxt").val().length > 0)
        {
            $this.attr("disabled","disabled");
            $.post("/dbms/connection/sql", {
                "sql" : $("#sqlQueryTxt").val()
            }, function(response){
                if (response.status == "success")
                {
                    var columns = response.data.columns;
                    var result = response.data.result;

                    // console.log("populating columns...");
                    // Populate Table columns with labels

                    if (columns.length > 0)
                    {
                       for (var col = 0; col < columns.length; col++)
                        {
                            var th = $("<th>" + columns[col] + "</th>")
                            $($("#resultTable thead:first").find("tr:first")).append(th);
                        }
                        $("#saveSqlBtn").removeAttr("disabled");
                        $("#updateSqlBtn").removeAttr("disabled");
                    }
                    else
                    {
                        var th = $("<th>No data</th>");
                        $($("#resultTable thead:first").find("tr:first")).append(th);

                        $("#saveSqlBtn").attr("disabled","disabled");
                        $("#updateSqlBtn").removeAttr("disabled");
                    }
                    // console.log("populating records...")
                    // Populate table records

                    for (var i = 0; i < result.length; i++)
                    {
                        var row = $("<tr></tr>");
                        for (var index in result[i])
                        {
                            console.log(result[i][index]);
                            var rowData = $("<td>" + result[i][index] + "</td>");
                            $(row).append(rowData);
                        }
                        $("#resultTable tbody:first").append(row);
                    }
                    // console.log("done...");

                    // set data for save button
                    $.data(elem, "sqlResult", columns.length);
                    $.data(updateElem, "sqlResult", columns.length);
                }
                else
                {
                    if (response.message != undefined)
                        Messenger().post(response.message);

                    if (response.data != undefined)
                    // set data for save button
                        $.data(elem, "sqlResult", response.data.columns.length);
                }

                $this.removeAttr("disabled");
            }).error(function(){
                Messenger().post("Unable to contact server");
                $this.removeAttr("disabled");
            });
        }

    });

        // Save query to table
    elem.on("click", function(){
        var iGood = $.data(elem, "sqlResult");
        $("#processStoreQueryModalBtn").attr("data-igood", iGood);

        if (iGood == 0)
        {
            Messenger().post("No 'select' queries were executed");
        } else
        {
            $("#storedQueryKey").val("");
            $("#storeQueryModal").modal();
        }
    });

    updateElem.on("click", function(){
        var iGood = $.data(updateElem, "sqlResult");
        if (iGood == 0)
        {
            Messenger().post("No 'select' queries were executed");
        }
        else
        {
            // Perform update
            var updateObj = $($(".storedQuery").filter(function(){
                return $(this).hasClass("active");
            }).first());
            var id = updateObj.attr("data-store-id");

            $.ajax({
                url: "/dbms/connection/query/update/" + id,
                cache: false,
                type: "PUT",
                data: {
                    "value" : $("#sqlQueryTxt").val().trim()
                },
                before_send: function(){

                },
                error: function(){
                    Messenger().post("Unable to contact server");
                },
                success: function(response){
                    if (response.status == "success")
                    {
                        Messenger().post("The stored query was successfully updated");
                    }
                    else
                    {
                        Messenger().post("Unable to update the stored query");
                    }
                }
            });
        }
    });

    $("#processStoreQueryModalBtn").on("click", function(){
        var iGood = parseInt($(this).attr("data-igood"));

        if ($("#storedQueryKey").val().length > 0)
        {
            if (const_key_regex.test($("#storedQueryKey").val()))
            {
                if (iGood > 0)
                {
                    $.post("/dbms/connection/query/store", {
                        "key" : $("#storedQueryKey").val().trim(),
                        "value" : $("#sqlQueryTxt").val().trim()
                    }, function(response){
                        if (response.status == "success")
                        {
                            $("#storeQueryModal").modal("hide");
                            $("#saveSqlBtn").attr("disabled","disabled");
                            $("#saveSqlBtn").show();
                            $("#updateSqlBtn").attr("disabled","disabled");
                            $("#updateSqlBtn").hide();
                            $($("#resultTable thead:first").find("tr:first")).html("");
                            $("#resultTable tbody:first").html("");
                            reloadStoredQueriesList();
                            Messenger().post("Query has been successfully stored");
                        } else
                        {
                            Messenger().post(response.message);
                        }
                    }).error(function(){
                        Messenger().post("Unable to contact server");
                    });
                }
                else
                {
                    Messenger().post("Query has no results");
                }
            }
            else
            {
                Messenger().post("stored query key is invalid");
            }
        }
        else
        {
            Messenger().post("stored query key is required");
        }
    });

    $("#sqlQueryTxt").on("change", function(){
        // Unimplemented
    });

    $("#sqlQueryTxt").on("keyup", function(){
        $("#saveSqlBtn").attr("disabled","disabled");
        $("#updateSqlBtn").attr("disabled","disabled");
    });

    $("#storeQueryList").on("click", ".storedQuery", function(e){
        e.stopPropagation();

        $(this).siblings().removeClass("active");
        if ($(this).hasClass("active"))
        {
            $(this).removeClass("active");
            $("#sqlQueryTxt").val("");
            $("#updateSqlBtn").hide();
            $("#localeSqlBtn").hide();
            $("#saveSqlBtn").show();
            $("#saveSqlBtn").attr("disabled","disabled");
            $($("#resultTable thead:first").find("tr:first")).html("");
            $("#resultTable tbody:first").html("");
        }
        else
        {
            $(this).addClass("active");
            $("#resultTable tbody:first").html("");
            $($("#resultTable thead:first").find("tr:first")).html("");
            $("#updateSqlBtn").attr("disabled","disabled");
            $("#updateSqlBtn").show();
            $("#localeSqlBtn").show();
            $("#saveSqlBtn").hide();

            var id = $(this).attr("data-store-id");
            $.getJSON("/dbms/connection/query/one/" + id + "/dbms_stored_query", function(response){
                if (response.status == "success")
                {
                    var data = JSON.parse(response.data.value);
                    $("#sqlQueryTxt").val(data.sql);
                } else
                {
                    Messenger().post("Unable to get stored query with that key");
                }
            }).error(function(){
                Messenger().post("Unable to contact server");
            });
        }
    });

    $("#storeQueryLanguageModal").on("shown", function(){
        $("#storeQueryLanguageSelect").attr('disabled','disabled');

        $.getJSON("/manager/s/items/list/locale", function(response){
            $("#storeQueryLanguageSelect").append($('<option value="none" data-value=""></option>'));

            for (var i = 0; i < response.length; i++)
            {
                var langItem = $("<option></option>");
                langItem.attr("value", response[i].value);
                langItem.attr("data-value", response[i].id);
                langItem.text(response[i].key);

                $("#storeQueryLanguageSelect").append(langItem);
            }

            var updateObj = $($(".storedQuery").filter(function(){
                return $(this).hasClass("active");
            }).first());
            var currentLanguageKey = updateObj.attr("data-store-language");

            $("#storeQueryLanguageSelect").val(currentLanguageKey);
            $("#storeQueryLanguageSelect").removeAttr("disabled");
        }).error(function(){
            $("#storeQueryLanguageSelect").append($('<option value="" data-value="">error when retrieving</option>'));
        });
    });

    $("#storeQueryLanguageModal").on("hidden", function(){
        $("#storeQueryLanguageSelect").html("");
    });

    $("#storeQueryLanguageSelect").on("change", function(){
        var languageKey = $(this).val();

        var updateObj = $($(".storedQuery").filter(function(){
            return $(this).hasClass("active");
        }).first());
        var id = updateObj.attr("data-store-id");

        $.ajax({
            url: "/dbms/connection/query/update/" + id,
            cache: false,
            type: "PUT",
            data: {
                "language" : languageKey
            },
            before_send: function(){

            },
            error: function(){
                Messenger().post("Unable to contact server");
            },
            success: function(response){
                if (response.status == "success")
                {
                    updateObj.attr("data-store-language",languageKey);
                    Messenger().post("The stored query was successfully updated");
                }
                else
                {
                    Messenger().post("Unable to update the stored query");
                }
            }
        });

    });

    $("#localeSqlBtn").on("click", function(){
        $("#storeQueryLanguageModal").modal();
        $("#storeQueryLanguageModal").modal("show");
    });

    $("#uninstallBtn").on("click", function(){
        var $this = $(this);
        $this.attr("disabled","disabled");
        $.post("/dbms/installer/uninstall", function(response){
            if (response.status == "success")
            {
                Messenger().post("Successfully uninstalled, redirecting...");
                location.reload();
            }
            else
            {
                $this.removeAttr("disabled");
                Messenger().post("Unable to uninstall component");
            }
        }).error(function(){
            Messenger().post("Unable to contact server");
            $this.removeAttr("disabled");
        })
    });
});