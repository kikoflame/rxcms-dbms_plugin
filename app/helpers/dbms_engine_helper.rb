module DbmsEngineHelper

  def persist_connection
    SymmetricEncryption.load!
    dbms_config = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/dbms/dbms_config.yml', __FILE__))))

    if (ActiveRecord::Base.connection == Database.connection)
      adapter = !Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_ADAPTER], session[:accessible_appid]] }).nil? ? Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_ADAPTER], session[:accessible_appid]] }).value : nil

      if (!adapter.nil?)
        if (adapter == 'sqlite')
          if (Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).nil?)
            raise "database malformed"
          end

          connectionResult = Database.connect_sqlite(Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).value)

          if (!connectionResult)
            raise "connection malformed or altered"
          end
        else
          if (Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_HOST], session[:accessible_appid]] }).nil? ||
              Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PORT], session[:accessible_appid]] }).nil? ||
              Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).nil? ||
              Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_USER], session[:accessible_appid]] }).nil? ||
              Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PASSWORD], session[:accessible_appid]] }).nil?)
            raise "host, database, user or password malformed"
          end

          connectionResult = Database.connect_sql(Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_ADAPTER], session[:accessible_appid]] }).value,
                                                  Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_HOST], session[:accessible_appid]] }).value,
                                                  Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PORT], session[:accessible_appid]] }).value,
                                                  Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_USER], session[:accessible_appid]] }).value,
                                                  Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_PASSWORD], session[:accessible_appid]] }).value,
                                                  Metadata.first({ :conditions => ['key = ? and sites_id = ?', dbms_config[:DBMS_CURRENT_DATABASE], session[:accessible_appid]] }).value)

          if (!connectionResult)
            raise "connection malformed or altered"
          end
        end
      else
        raise "adapter malformed"
      end
    else
      logger.debug("[dbms-plugin] Not persisting connection")
    end

  end

end
