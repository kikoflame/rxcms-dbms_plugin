$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rxcms-dbms_plugin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rxcms-dbms_plugin"
  s.version     = RxcmsDbmsPlugin::VERSION
  s.authors     = ["Anh Nguyen","Julian Cowan"]
  s.email       = ["rxcms@ngonluakiko.com"]
  s.homepage    = "https://bitbucket.org/kikoflame/rxcms-dbms_plugin"
  s.summary     = "Ability to include content from external data sources."
  s.description = "Plugin for RXCMS which provides a basic sql interface for DBMS"
  s.licenses    = ["MIT"]
  

  s.metadata = {
      "compatCode" => "2dd89a9ecdeb920d539954fd9fb3da7de9f52a61ee9895d06e4bc1bab11a598b",
      "fullName" => "DBMS Connector",
      "mountPoint" => "/dbms/engine/configure",
      "installerPoint" => "/dbms/engine/installer"
  }

  s.files = Dir["{app,config,db,lib}/**/*"] + ["LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.2.13"
  s.add_dependency "sqlite3"
  s.add_dependency "mysql2"
  s.add_dependency "pg"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "mysql2"
  s.add_development_dependency "pg"
end
